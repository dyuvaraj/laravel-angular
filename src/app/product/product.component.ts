import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  productData:any = []; 

  constructor(private _auth: AuthService, private _router: Router) { }

  cartAmt = 0;

  ngOnInit(): void {
    this._auth.productShowall().subscribe(
      response => {
        this.productData = response.product;
        console.log(this.productData);
      }
    );
  }

  calculateCart(amt: any){
    this.cartAmt = this.cartAmt + amt;
  }



  
}
