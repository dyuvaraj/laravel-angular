import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['../app.component.css']
})
export class ContactComponent implements OnInit {

  constructor(private _auth: AuthService, private _router: Router) { }

  contactData:any = {};
  ngOnInit(): void {
  }

  contactAdd(){
    this._auth.contactAdd(this.contactData)
    .subscribe(
      res => {console.log(res)
        this._router.navigate(['/contact']);
      },
      err => console.log(err),
    );
  }

}
