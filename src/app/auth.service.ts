import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _registerUrl = "http://127.0.0.1:8000/api/auth/register";
  private _loginUrl = "http://127.0.0.1:8000/api/auth/login";
  private _productaddUrl = "http://127.0.0.1:8000/api/auth/product/add";
  private _productImageaddUrl = "http://127.0.0.1:8000/api/auth/product/imageupload";
  private _productshowallUrl = "http://127.0.0.1:8000/api/auth/product/showall";
  private _contactSaveUrl = "http://127.0.0.1:8000/api/auth/contact/save";

  constructor(private http: HttpClient, private _router: Router) { }

  contactAdd(product: any){
    return this.http.post<any>(this._contactSaveUrl, product);
  }

  productAdd(product: any, headers:HttpHeaders){
    return this.http.post<any>(this._productaddUrl, product, {headers:headers});
  }

  productImageAdd(product: any){
    return this.http.post<any>(this._productImageaddUrl, product);
  }

  productShowall(){
    return this.http.get<any>(this._productshowallUrl);
  }

  registerUser(user: any){
    return this.http.post<any>(this._registerUrl, user);
  }

  loginUser(user: any){
    return this.http.post<any>(this._loginUrl, user);
  }

  loggedIn(){
    return !!localStorage.getItem('token');
  }

  logoutUser(){
    localStorage.removeItem('token');
    this._router.navigate(['/product-view']);
  }

  getToken(){
    return localStorage.getItem('token');
  }
}
