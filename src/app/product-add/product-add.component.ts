import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['../app.component.css']
})
export class ProductAddComponent implements OnInit {

  filedata:any;
  
  fileEvent(e: any ){
        this.filedata = e.target.files[0];
  }

  constructor(private _auth: AuthService, private _router: Router) { }

  productData:any = {};
  ngOnInit(): void {
  }

  productAdd(){


    var myFormData = new FormData();
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');

    myFormData.append('name', this.productData.name);
    myFormData.append('price', this.productData.price);
    myFormData.append('quantity', this.productData.quantity);
    myFormData.append('description', this.productData.description);
    myFormData.append('file_thumbnail_image', this.filedata);


    this._auth.productAdd(myFormData, headers)
    .subscribe(
      res => {console.log(res)
        this._router.navigate(['/product-add']);
      },
      err => console.log(err),
    );
  }


}
