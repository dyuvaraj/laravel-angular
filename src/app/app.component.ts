import { Component } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  CartValueAdded: any = 0;
  title = 'code-ang';
  constructor(public _authService: AuthService){ }

  UpdateCart(amt: any){
    this.CartValueAdded = amt;
  }

}
